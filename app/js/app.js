import Template from './template';
import Form from './form';
import Popup from './popup';

const templateCrtl = new Template();
templateCrtl.makeRequest('header.tpl');

const formCrtl = new Form({});
const popupCrtl = new Popup();


const saveBtn = document.querySelector('#save');
const cancelBtn = document.querySelector('#cancel');
const editIcon = document.querySelector('#edit_icon');

const editBtnName = document.querySelector('#edit_name');
const editBtnWebsite = document.querySelector('#edit_website');
const editBtnPhone = document.querySelector('#edit_phone');
const editBtnLocation = document.querySelector('#edit_location');

const settings = document.querySelector('#settings');
const about = document.querySelector('#about');

const popupSave = document.querySelector('#popup_save');
const popupCancel = document.querySelector('#popup_cancel');

let nextRef = "";

saveBtn.addEventListener('click', (event) => {
    event.preventDefault();
    formCrtl.saveData();
});


cancelBtn.addEventListener('click', (event) => {
    event.preventDefault();
    formCrtl.hideForm();
});


editIcon.addEventListener('click', () => {
   formCrtl.showForm();
});


editBtnName.addEventListener('click', (event) => {
    popupCrtl.showPopup(editBtnName, 'name');
    nextRef = editBtnName.previousElementSibling;
});

editBtnLocation.addEventListener('click', (event) => {
    popupCrtl.showPopup(editBtnLocation, 'city, state & zip');
    nextRef = editBtnLocation.previousElementSibling;
});

editBtnPhone.addEventListener('click', (event) => {
    popupCrtl.showPopup(editBtnPhone, 'Phone Number');
    nextRef = editBtnPhone.previousElementSibling;
});

editBtnWebsite.addEventListener('click', (event) => {
    popupCrtl.showPopup(editBtnWebsite, 'website');
    nextRef = editBtnWebsite.previousElementSibling;
});

popupSave.addEventListener('click', (event) => {
    event.preventDefault();
    popupCrtl.save(nextRef);
});

popupCancel.addEventListener('click', (event) => {
    event.preventDefault();
    popupCrtl.hidePopup();
});
