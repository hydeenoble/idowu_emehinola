export default class Form{

    constructor(data) {
        this.data = data;
    }

    showForm(){
        this.storePrevDetails();
        this.populateForm();

        const form = document.querySelector('#form');
        const aboutList = document.querySelector('#about__list');
        const editIcon = document.querySelector('#edit_icon');
        const headingBtn = document.querySelector('#heading_btn');

        aboutList.style.display = 'none';
        form.style.display = 'block';
        editIcon.style.display = 'none';
        headingBtn.style.display = 'flex';

    }

    hideForm(){
        const form = document.querySelector('#form');
        const aboutList = document.querySelector('#about__list');
        const editIcon = document.querySelector('#edit_icon');
        const headingBtn = document.querySelector('#heading_btn');


        form.style.display = 'none';
        aboutList.style.display = 'block';
        headingBtn.style.display = "none";
        editIcon.style.display = 'inline-block';
    }

    saveData(){
        const fname = document.querySelector('#fname');
        const lname = document.querySelector('#lname');
        const website = document.querySelector('#website');
        const phone = document.querySelector('#phone_number');
        const city = document.querySelector('#city');

        const dataName = document.querySelector('#data_name');
        const dataWebsite = document.querySelector('#data_website');
        const dataPhone = document.querySelector('#data_phone');
        const dataLocation = document.querySelector('#data_location');

        let inputs = {};

        inputs['fname'] = fname.value
        inputs['lname'] = lname.value
        inputs['website'] = website.value;
        inputs['phone'] = phone.value;
        inputs['location'] = city.value;

        dataName.textContent = `${inputs.fname} ${inputs.lname}`;
        dataWebsite.textContent = inputs.website;
        dataPhone.textContent = inputs.phone;
        dataLocation.textContent = inputs.location;

        this.hideForm();
    }


    storePrevDetails(){
        const dataName = document.querySelector('#data_name');
        const dataWebsite = document.querySelector('#data_website');
        const dataPhone = document.querySelector('#data_phone');
        const dataLocation = document.querySelector('#data_location');

        let name = dataName.textContent;
        let nameArray = name.split(' ');

        this.data['fname'] = nameArray[0];
        this.data['lname'] = nameArray[1];
        this.data['website'] = dataWebsite.textContent;
        this.data['phone'] = dataPhone.textContent;
        this.data['location'] = dataLocation.textContent;
    }

    populateForm(){
        const fname = document.querySelector('#fname');
        const lname = document.querySelector('#lname');
        const website = document.querySelector('#website');
        const phone = document.querySelector('#phone_number');
        const city = document.querySelector('#city');

        fname.value = this.data.fname;
        lname.value = this.data.lname;
        website.value = this.data.website;
        phone.value = this.data.phone;
        city.value = this.data.location;
    }
}