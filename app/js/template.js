export default class Template{

    constructor(){}

    makeRequest(file){

        let url = `./app/partials/${file}`;

        let xmlhttp = '';
        if (window.XMLHttpRequest) {
            // code for modern browsers
            xmlhttp = new XMLHttpRequest();
        } else {
            // code for old IE browsers
            xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
        }


        xmlhttp.onreadystatechange = function() {
            if (this.readyState == 4 && this.status == 200) {
                // console.log(this.responseText)
                document.querySelector('.header').innerHTML = this.responseText;
            }
        };


        xmlhttp.open("GET", url, true);
        xmlhttp.send();

    }

    clearActiveNav(){
        const navs = document.querySelectorAll('.nav__link');
        navs.forEach((nav) => {
            nav.classList.remove('nav__link--active')
        })
    }
}