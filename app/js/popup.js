export default class Popup{


    constructor(data){
        this.data = data;
        this.popup = document.querySelector('.popup');
        this.input = document.querySelector('#input')
        this.inputLabel = document.querySelector('#input_label')
    }

    showPopup(ref, label){
        this.popup.style.left = (ref.offsetLeft + 7) + 'px';
        this.popup.style.top = (ref.offsetTop  - 7) + 'px';
        this.popup.style.display = 'inline-block';
        this.input.value = ref.previousElementSibling.textContent;
        this.input.placeholder = label;
        this.inputLabel.textContent = label;
    }

    hidePopup(){
        this.popup.style.display = 'none';
    }

    save(ref){
        ref.textContent = this.input.value;
        this.popup.style.display = 'none';
    }
}