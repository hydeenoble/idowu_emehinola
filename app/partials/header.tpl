<div class="logout">
            <a href="#" class="logout__button">log out</a>
        </div>
        <div class="upload">
            <div class="upload__button">
                <span class="icon ion-ios-reverse-camera"></span>
                <p class="upload__button-text">Upload Cover Image</p>
            </div>

        </div>
        <div class="user">
            <div class="user__photo-box">
                <!--<img src="./img/profile_image.jpg" alt="User Photo" class="user__photo">-->
            </div>

            <div class="user__details-wrapper">
                <div class="user__details">
                    <h3 class="user__name">Jessica Parker</h3>

                    <div class="user__location">
                        <span class="user__location-icon icon ion-ios-location-outline"></span>
                        <p class="user__location-text">Newport Beach, CA</p>
                    </div>

                    <div class="user__phone">
                        <span class="user__phone-icon icon ion-ios-telephone-outline"></span>
                        <p class="user__phone-text">(949) 325-68594</p>
                    </div>
                </div>

                <div class="user__rate">
                    <div class="user__rate-icon">
                        <span class="icon ion-ios-star"></span>
                        <span class="icon ion-ios-star"></span>
                        <span class="icon ion-ios-star"></span>
                        <span class="icon ion-ios-star"></span>
                        <span class="icon ion-ios-star-outline"></span>
                    </div>
                    <p class="user__rate-review">
                        6 Reviews
                    </p>
                </div>
            </div>

        </div>
        <nav class="nav">
            <ul class="nav__list">
                <li class="nav__item"><a href="index.html" class="nav__link nav__link--active" id="about">About</a></li>
                <li class="nav__item"><a href="settings.html" class="nav__link" id="settings">Settings</a></li>
                <li class="nav__item"><a href="#" class="nav__link">Option1</a></li>
                <li class="nav__item"><a href="#" class="nav__link">Option2</a></li>
                <li class="nav__item"><a href="#" class="nav__link">Option3</a></li>
            </ul>

            <div class="nav__followers">
                <div class="nav__followers-icon">
                    <span class="icon ion-ios-plus"></span>
                </div>
                <p class="nav__followers-text">
                    <span>15</span> Followers
                </p>
            </div>
        </nav>