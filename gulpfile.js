const gulp = require('gulp');
const sass = require('gulp-sass');
const browserSync = require('browser-sync').create();

const paths = {
    scssFiles: "app/scss/**/*.scss",
    cssFiles: "app/css",
    partials: "app/partials/*.tpl",
    htmlFiles: "*.html",
    jsFiles: "app/js/**/*.js",
    bundle: "dist/bundle.js"
}

gulp.task('sass', () => {
    gulp.src(`${paths.scssFiles}`)
        .pipe(sass())
        .pipe(gulp.dest(`${paths.cssFiles}`))
        .pipe(browserSync.reload({
            stream: true
        }))
});

gulp.task('watch',['browserSync', 'sass'], () => {
    gulp.watch(`${paths.scssFiles}`, ['sass']);
    gulp.watch(`${paths.htmlFiles}`, browserSync.reload);
    gulp.watch(`${paths.partials}`, browserSync.reload);
    gulp.watch(`${paths.bundle}`, browserSync.reload);
    // gulp.watch('app/js/**/*.js', browserSync.reload);
});

gulp.task('browserSync', () => {
    browserSync.init({
        server: {
            baseDir: './'
        },
    })
});